// JS
$(document).ready(function(){
    $(".royalSlider").royalSlider({
    	autoScaleSlider: true,
    	autoHeight: false,
    	arrowsNav: true,
        globalCaption: false,
        controlNavigation: 'none',
        imageScaleMode: 'none',
        imageAlignCenter: false,
        keyboardNavEnabled: true,
        slidesSpacing: 0,
        usePreloader: false,
        transitionSpeed: 600,
        transitionType: 'fade',
        sliderDrag: true,
        loop: true,
        loopRewind: true,
        autoPlay: {
    		enabled: true,
            delay: 6000,
    		pauseOnHover: false
    	}
    });
});
